import 'dart:convert';
import 'dart:io';
import 'dart:math' as math;

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:leanchat/data/login/fb_token.dart';
import 'package:leanchat/data/login/in_memory_token_storage.dart';
import 'package:leanchat/data/login/token.dart';
import 'package:leanchat/data/user/in_memory_user_storage.dart';
import 'package:leanchat/data/user/user.dart';

import 'package:firebase_storage/firebase_storage.dart';

class LeanchatClient {
  static const _BASE_URL = 'https://api.preview.leanchat.aks.lncd.pl/';
  static const facebookGrantName = 'facebook';

  static final tokenStorage = InMemoryTokenStorage();
  static final userStorage = InMemoryUserStorage();

  static Future<Response> signIn(String fbToken) async {
    final result = await _signInWithFacebook(fbToken);

    if (result.statusCode == HttpStatus.ok) {
      final token = Token.fromJson(json.decode(result.body));
      tokenStorage.setToken(token);
      final firebaseResponse = await _getFirebaseToken();
      if (firebaseResponse.statusCode == HttpStatus.ok) {
        return await _getCurrentUser();
      } else {
        return firebaseResponse;
      }
    } else if (result.statusCode == HttpStatus.badRequest) {
      final body = json.decode(result.body);
      if (body['error_description'] == "user_does_not_exist") {
        final registerResponse = await _register(fbToken);
        if (registerResponse.statusCode == HttpStatus.ok) {
          return await _signInWithFacebook(fbToken);
        } else {
          return registerResponse;
        }
      }
    }

    return result;
  }

  static Future<Response> getGif(String phrase) async {
    return await _get(_BASE_URL + 'giphy?phrase=$phrase');
  }

  static Future<String> saveImage(File image) async {
    FirebaseStorage _storage = FirebaseStorage.instance;
    StorageReference reference =
        _storage.ref().child("images/${math.Random().nextInt(999999)}");
    StorageUploadTask uploadTask = reference.putFile(image);

    var loc = await uploadTask.onComplete;
    String location = await reference.getDownloadURL();
    return location;
  }

  static Future<Response> getChannel(String channelId) async {
    return await _get(_BASE_URL + 'channels/$channelId');
  }

  static Future<Response> setMessageStatus(
      String channelId, String messageId) async {
    final body = {'seen': true};
    return await _post(
        _BASE_URL + 'channels/$channelId/messages/$messageId', body);
  }

  static Future<Response> _signInWithFacebook(String fbToken) async {
    final body = {
      'scope': 'leanchat/api openid profile email offline_access',
      'grant_type': facebookGrantName,
      'client_id': 'leanchat',
      'token': fbToken
    };
    return await http.post(
        'https://api.preview.leanchat.aks.lncd.pl/connect/token',
        headers: {"Content-Type": "application/x-www-form-urlencoded"},
        body: body);
  }

  static Future<Response> _register(String fbToken) {
    final body = {'access_token': fbToken};

    return _put(_BASE_URL + 'user/facebook', body);
  }

  static Future<Response> _getFirebaseToken() async {
    final firebaseTokenResponse = await _get(_BASE_URL + "me/firebase-token");
    if (firebaseTokenResponse.statusCode == HttpStatus.ok) {
      final fbToken = FbToken.fromJson(json.decode(firebaseTokenResponse.body));
      tokenStorage.setFbToken(fbToken);
    }
    return firebaseTokenResponse;
  }

  static Future<Response> _getCurrentUser() async {
    final response = await _get(_BASE_URL + "me");
    if (response.statusCode == HttpStatus.ok) {
      final user = User.fromJson(json.decode(response.body));
      userStorage.setUser(user);
    }
    return response;
  }

  static Future<Response> _put(String url, Map<String, dynamic> body) {
    return http.put(url,
        headers: {"Content-Type": "application/json"}, body: json.encode(body));
  }

  static Future<Response> _post(String url, Map<String, dynamic> body) async {
    final token = await tokenStorage.getToken();
    return http.post(url,
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ${token.accessToken}'
        },
        body: json.encode(body));
  }

  static Future<Response> _get(String url) async {
    final token = await tokenStorage.getToken();
    try {
      return http
          .get(url, headers: {'Authorization': 'Bearer ${token.accessToken}'});
    } catch (ex) {}
  }
}
