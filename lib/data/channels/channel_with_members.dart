import 'member.dart';

class ChannelWithMembers {
  factory ChannelWithMembers.fromJson(Map<String, dynamic> json) {
    final membersJson = json['members'] as List;
    final members = membersJson
        .map((item) => Member.fromJson(item))
        .toList(growable: false)
          ..sort((a, b) => a.name.compareTo(b.name));

    return ChannelWithMembers(
        id: json['id'], name: json['name'], members: members);
  }

  ChannelWithMembers({this.id, this.name, this.members});

  final String id;
  final String name;
  final List<Member> members;
}
