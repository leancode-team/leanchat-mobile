import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leanchat/data/channels/member.dart';

class Channel {
  factory Channel(DocumentSnapshot doc, List<Member> members) {
    if (doc == null || !doc.exists) {
      return null;
    }

    final docMembers = doc.data.containsKey('members') ? Map<String, dynamic>.from(doc.data['members']) : Map();

    return Channel._new()
      ..id = doc.documentID
      ..name = doc.data['name']
      ..photo = doc.data['photo']
      ..members = members.map((member) => Member.withSeenMessageId(member, docMembers[member.id])).toList(growable: false);
  }

  Channel._new();

  String id;
  String name;
  String photo;
  List<Member> members;
}
