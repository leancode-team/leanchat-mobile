
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leanchat/data/login/in_memory_token_storage.dart';

class FirestoreProvider {

  static final tokenStorage = InMemoryTokenStorage();

  static CollectionReference getMessagesDocuments(String channelId) {
    return Firestore.instance
        .collection('user_id/${tokenStorage.getAuthor()}/channels/$channelId/messages');
  }

  static CollectionReference getChannelsDocuments() {
    return Firestore.instance.collection('user_id/${tokenStorage.getAuthor()}/channels');
  }

  FirestoreProvider._new();
}