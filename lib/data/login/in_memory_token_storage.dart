import 'package:leanchat/data/login/fb_token.dart';
import 'package:leanchat/data/login/token.dart';

class InMemoryTokenStorage {

  static final InMemoryTokenStorage _inMemoryTokenStorage = InMemoryTokenStorage._new();

  factory InMemoryTokenStorage() {
    return _inMemoryTokenStorage;
  }

  InMemoryTokenStorage._new();

  Token _token;
  FbToken _fbToken;

  Future<Token> getToken() {
    return Future.value(_token);
  }

  Future<void> setToken(Token token) {
    return Future.sync(() {
      _token = token;
    });
  }

  String getAuthor() {
    return _fbToken.author;
  }

  Future<void> setFbToken(FbToken token) {
    return Future.sync(() {
      _fbToken = token;
    });
  }

  Future<void> wipeTokens() {
    return Future.sync(() {
      _token = null;
      _fbToken = null;
    });
  }
}
