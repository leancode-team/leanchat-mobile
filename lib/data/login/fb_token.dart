class FbToken {
  factory FbToken.fromJson(Map<String, dynamic> json) {
    return FbToken(
        accessToken: json['access_token'], author: json['author']);
  }

  FbToken({this.accessToken, this.author});

  String accessToken;
  String author;
}
