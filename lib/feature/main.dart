import 'package:flutter/material.dart';
import 'package:leanchat/feature/channels/channels_model.dart';
import 'package:leanchat/feature/login/login_model.dart';
import 'package:leanchat/feature/login/login_page.dart';
import 'package:provider/provider.dart';

import 'channels/channels_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ListenableProvider<LoginModel>.value(value: LoginModel()),
        ListenableProvider<ChannelsModel>.value(value: ChannelsModel()),
      ],
      child: MaterialApp(
        title: 'Leanchat',
        theme: ThemeData(
          primaryColor: Colors.white,
          secondaryHeaderColor: Colors.grey,
          textTheme: TextTheme(
            body1: TextStyle(color: Colors.white),
          ),
        ),
        home: LoginPage(),
      ),
    );
  }
}
