import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:leanchat/data/network/leanchat_client.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginModel extends Model {
  Status _status = const Empty();

  Status get status => _status;

  final ChangeNotifier loggedInNotifier = ChangeNotifier();

  Future login() async {
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logInWithReadPermissions(['email']);

    if (result.status == FacebookLoginStatus.loggedIn) {
      final response = await LeanchatClient.signIn(result.accessToken.token);
      if (response.statusCode == HttpStatus.ok) {
        _status = LoggedIn();
        loggedInNotifier.notifyListeners();
      } else {
       _status = Error("${response.statusCode}: ${response.reasonPhrase}: ${        response.body}");
      }
    } else if (result.status == FacebookLoginStatus.cancelledByUser) {
      _status = Error("Cancelled by user");
    } else {
      _status = Error(result.errorMessage);
    }
    notifyListeners();
  }
}

abstract class Status {}

class Empty implements Status {
  const Empty();
}

class LoggedIn implements Status {
  const LoggedIn();
}

class Error implements Status {
  final error;

  Error(this.error);
}
