import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:leanchat/feature/channels/channels_model.dart';
import 'package:leanchat/feature/channels/channels_page.dart';
import 'package:leanchat/feature/login/login_model.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginModel model;

  @override
  void initState() {
    super.initState();

    // _model = LoginModel();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _navigationListener() {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => ChannelsPage()));
  }

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<LoginModel>(context);
    model.loggedInNotifier.removeListener(_navigationListener);

    model.loggedInNotifier.addListener(_navigationListener);

    return LoginForm();
  }
}

class LoginForm extends StatelessWidget {
  const LoginForm({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<LoginModel>(context);
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 40,
              child: SignInButton(
                Buttons.Facebook,
                onPressed: () {
                  model.login();
                },
              ),
            ),
            if (model.status is Error)
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 0.0),
                child: Text(
                  (model.status as Error).error,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Colors.red,
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
