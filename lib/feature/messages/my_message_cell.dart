import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:leanchat/data/channels/channel.dart';
import 'package:leanchat/data/messages/message.dart';
import 'package:leanchat/utils/design_helpers.dart';
import 'package:intl/intl.dart';

class MyMessageCell extends StatelessWidget {
  const MyMessageCell({Key key, @required this.message, @required this.channel})
      : super(key: key);

  final Message message;
  final Channel channel;

  @override
  Widget build(BuildContext context) {
    final user =
        channel.members.firstWhere((member) => member.id == message.from);
    final seen = channel.members
        .where((member) => member.seenMessageId == message.id)
        .map((member) => member.photo)
        .toList(growable: false);
    final subtitleStyle = TextStyle(fontSize: 10, color: Colors.grey);
    final subtitle = user.name + ", " + _dateFormat.format(message.timestamp);
    return Container(
      margin: const EdgeInsets.fromLTRB(16, 8, 16, 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // User avatar
          Container(
            margin: const EdgeInsets.fromLTRB(0, 24, 0, 0),
            child: CircleAvatar(
              backgroundImage: CachedNetworkImageProvider(user.photo),
              radius: 20,
            ),
          ),
          // Message
          Flexible(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
              child: IntrinsicWidth(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(8, 0, 0, 4),
                      child: Text(subtitle, style: subtitleStyle),
                    ),
                    message.hasMessage()
                        ? _createMessageWidget(message.message, context)
                        : _createGifWidget(message.gif),
                    // Last seen
                    Wrap(
                      alignment: WrapAlignment.end,
                      children: [
                        for (var i = 0; i < seen.length; i++)
                          Container(
                            padding: const EdgeInsets.all(4),
                            child: CircleAvatar(
                              backgroundImage: NetworkImage(seen[i]),
                              radius: 8,
                            ),
                          )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _createMessageWidget(String message, BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        Clipboard.setData(new ClipboardData(text: message));
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Copied to clipboard'),
        ));
      },
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.green,
            boxShadow: [DesignHelpers.getShadow()]),
        child: MarkdownBody(
          data: message,
        ),
      ),
    );
  }

  Widget _createGifWidget(String gif) {
    return ClipRRect(
      borderRadius: new BorderRadius.circular(8.0),
      child: CachedNetworkImage(
        imageUrl: gif,
        placeholder: (context, url) => Center(
              child: Padding(
                padding: EdgeInsets.all(12.0),
                child: CircularProgressIndicator(),
              ),
            ),
        errorWidget: (context, url, error) => new Icon(Icons.error),
      ),
    );
  }
}

final DateFormat _dateFormat = DateFormat.yMd().add_jm();
