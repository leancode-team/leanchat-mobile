import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leanchat/data/firebase/firestore_provider.dart';
import 'package:leanchat/data/messages/message.dart';
import 'package:leanchat/data/network/leanchat_client.dart';
import 'package:leanchat/data/user/in_memory_user_storage.dart';
import 'package:meta/meta.dart';
import 'package:scoped_model/scoped_model.dart';

class MessagesModel extends Model {
  MessagesModel({@required String channelId})
      : _channelId = channelId {
    _subscription = FirestoreProvider.getMessagesDocuments(_channelId)
        .snapshots()
        .listen((_) => fetchMessages());
  }

  static const String _GIF_PHRASE = "test";

  final String _channelId;
  final userStorage = InMemoryUserStorage();
  StreamSubscription<QuerySnapshot> _subscription;

  List<Message> _messages = [];

  List<Message> get messages => _messages;

  Future<void> fetchMessages() async {
    final userId = (await userStorage.getUser()).id;
    final docs =
        await FirestoreProvider.getMessagesDocuments(_channelId).getDocuments();

    _messages = docs.documents
        .map((doc) => Message.fromSnapshot(doc, userId))
        .toList(growable: false);

    _messages.sort((a, b) => b.timestamp.compareTo(a.timestamp));

    final lastSeenMessage = _messages.first;
    await LeanchatClient.setMessageStatus(_channelId, lastSeenMessage.id);
    notifyListeners();
  }

  Future<void> sendMessage(String message) async {
    final userId = (await userStorage.getUser()).id;
    final messageMap = Message.fromData(userId, message, null).toMap();

    await FirestoreProvider.getMessagesDocuments(_channelId).add(messageMap);

    fetchMessages();
  }

  Future<void> sendGif() async {
    final response = await LeanchatClient.getGif(_GIF_PHRASE);
    if (response.statusCode == HttpStatus.ok) {
      final userId = (await userStorage.getUser()).id;
      final body = response.body;
      final gif = json.decode(body)["url"];
      final messageMap = Message.fromData(userId, null, gif).toMap();

      await FirestoreProvider.getMessagesDocuments(_channelId)
          .add(messageMap);

      fetchMessages();
    }
  }

   Future<void> sendImage(File image) async {
    final response = await LeanchatClient.saveImage(image);
    if (response != null) {
      final userId = (await userStorage.getUser()).id;
      final messageMap = Message.fromData(userId, null, response).toMap();

      await FirestoreProvider.getMessagesDocuments(_channelId)
          .add(messageMap);

      fetchMessages();
    }
  }

  void dispose() {
    _subscription.cancel();
  }
}
