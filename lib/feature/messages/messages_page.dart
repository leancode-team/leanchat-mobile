import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:leanchat/data/channels/channel.dart';
import 'package:leanchat/feature/channels/channels_model.dart';
import 'package:leanchat/feature/messages/my_message_cell.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';

import 'message_cell.dart';
import 'messages_model.dart';
import 'send_message_field.dart';

class MessagesPage extends StatefulWidget {
  const MessagesPage({Key key, @required this.channelId}) : super(key: key);

  final String channelId;

  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  MessagesModel _messagesModel;

  @override
  void initState() {
    super.initState();

    _messagesModel = MessagesModel(channelId: widget.channelId);
    _messagesModel.fetchMessages();
  }

  @override
  void dispose() {
    super.dispose();
    _messagesModel.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final channelsModel = Provider.of<ChannelsModel>(context);
    if (channelsModel.channels.isNotEmpty)
      return _getMessagesPage(channelsModel, context);
    else
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
  }

  Widget _getMessagesPage(ChannelsModel channelsModel, BuildContext context) {
    final Channel channel =
        channelsModel.channels.firstWhere((c) => c.id == widget.channelId);
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text(channel.name),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.grey,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          actions: [
            MediaButton(messagesModel: _messagesModel),
          ],
        ),
        body: Container(
          decoration: BoxDecoration(color: Colors.grey.withAlpha(20)),
          child: Stack(
            children: <Widget>[
              RefreshIndicator(
                onRefresh: _messagesModel.fetchMessages,
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(vertical: 90.0),
                  reverse: true,
                  itemCount: _messagesModel.messages.length,
                  itemBuilder: (___, index) {
                    final message = _messagesModel.messages[index];

                    if (message.mine)
                      return MyMessageCell(message: message, channel: channel);
                    else
                      return MessageCell(message: message, channel: channel);
                  },
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: SendMessageField(model: _messagesModel),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MediaButton extends StatelessWidget {
  const MediaButton({
    Key key,
    @required MessagesModel messagesModel,
  })  : _messagesModel = messagesModel,
        super(key: key);

  final MessagesModel _messagesModel;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: IconButton(
        icon: Icon(
          FontAwesomeIcons.fileImage,
          size: 20,
          color: Colors.grey,
        ),
        onPressed: () async {
          await showDialog<void>(
            context: context,
            builder: (context) {
              return SimpleDialog(
                title: Text('Send image'),
                children: <Widget>[
                  FlatButton(
                    onPressed: () async {
                      _messagesModel.sendGif();
                      Navigator.pop(context);
                    },
                    child: Text('Send random Gif'),
                  ),
                  FlatButton(
                    onPressed: () async {
                      var image = await ImagePicker.pickImage(
                          source: ImageSource.camera);

                      if (image != null) {
                        _messagesModel.sendImage(image);
                      }
                      Navigator.pop(context);
                    },
                    child: Text('Send photo from camera'),
                  ),
                  FlatButton(
                    onPressed: () async {
                      var image = await ImagePicker.pickImage(
                          source: ImageSource.gallery);

                      if (image != null) {
                        _messagesModel.sendImage(image);
                      }
                      Navigator.pop(context);
                    },
                    child: Text('Send image from gallery'),
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }
}
