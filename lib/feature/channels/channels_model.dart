import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:leanchat/data/channels/channel.dart';
import 'package:leanchat/data/channels/channel_with_members.dart';
import 'package:leanchat/data/firebase/firestore_provider.dart';
import 'package:leanchat/data/network/leanchat_client.dart';
import 'package:scoped_model/scoped_model.dart';

class ChannelsModel extends Model {
  ChannelsModel() {}

  List<Channel> _channels = [];

  List<Channel> get channels => _channels;

  StreamSubscription<QuerySnapshot> _subscription;

  Future<void> fetchChannels() async {
    _subscription ??= FirestoreProvider.getChannelsDocuments()
        .snapshots()
        .listen((_) => fetchChannels());

    final docs = await FirestoreProvider.getChannelsDocuments().getDocuments();

    _channels = await Stream.fromIterable(docs.documents)
        .asyncMap((doc) => _getChannelWithMembers(doc))
        .toList()
      ..sort((a, b) => a.name.compareTo(b.name));

    notifyListeners();
  }

  void dispose() {
    _subscription.cancel();
  }

  Future<Channel> _getChannelWithMembers(DocumentSnapshot doc) async {
    return await LeanchatClient.getChannel(doc.documentID)
        .then((response) =>
            ChannelWithMembers.fromJson(json.decode(response.body)).members)
        .then((members) => Channel(doc, members));
  }
}
