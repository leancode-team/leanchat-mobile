import 'package:flutter/material.dart';
import 'package:leanchat/feature/channels/channel_list_tile.dart';
import 'package:provider/provider.dart';
import 'channels_model.dart';

class ChannelsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Center(
          child: const Text('Channels'),
        ),
      ),
      body: ChannelsBody(),
    );
  }
}

class ChannelsBody extends StatefulWidget {
  const ChannelsBody({
    Key key,
  }) : super(key: key);

  @override
  _ChannelsBodyState createState() => _ChannelsBodyState();
}

class _ChannelsBodyState extends State<ChannelsBody> {
  bool _visible;

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<ChannelsModel>(context);
    _visible = model.channels.isNotEmpty;
    model.fetchChannels();
    return Stack(
      children: <Widget>[
        AnimatedOpacity(
            duration: Duration(milliseconds: 600),
            opacity: _visible ? 1 : 0,
            child: ChannelsList(model: model)),
        IgnorePointer(
          child: AnimatedOpacity(
            duration: Duration(milliseconds: 600),
            opacity: _visible ? 0 : 1,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Loading...',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                  for (var i = 0; i < 5; i++) PlaceholderRow(),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class ChannelsList extends StatelessWidget {
  const ChannelsList({
    Key key,
    @required this.model,
  }) : super(key: key);

  final ChannelsModel model;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RefreshIndicator(
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: model.channels.length,
          itemBuilder: (context, index) {
            return ChannelListTile(channel: model.channels[index]);
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
        ),
        onRefresh: () async => await model.fetchChannels(),
      ),
    );
  }
}

class PlaceholderRow extends StatelessWidget {
  const PlaceholderRow({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: Row(
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Container(
                color: Colors.grey[400],
                width: 75,
                height: 56,
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Container(
                  color: Colors.grey[300],
                  height: 30,
                  width: 160,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Container(
                  color: Colors.grey[200],
                  height: 12,
                  width: 180,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
