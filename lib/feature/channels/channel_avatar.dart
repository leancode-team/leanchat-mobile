import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ChannelAvatar extends StatelessWidget {
  const ChannelAvatar(
    this.photo, {
    Key key,
  }) : super(key: key);

  final String photo;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ClipRRect(
      borderRadius: BorderRadius.circular(8.0),
      child: CachedNetworkImage(
        imageUrl: photo,
        placeholder: (context, url) => Padding(
            padding: EdgeInsets.all(4.0), child: CircularProgressIndicator()),
        errorWidget: (context, url, error) => new Icon(Icons.error),
      ),
    ));
  }
}
