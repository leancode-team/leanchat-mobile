import 'package:flutter/material.dart';
import 'package:leanchat/feature/channels/channels_model.dart';

class ChannelsModelProvider extends InheritedWidget {
  final ChannelsModel model;

  ChannelsModelProvider({
    Key key,
    @required this.model,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(ChannelsModelProvider old) => true;
}
