import 'package:flutter/material.dart';
import 'package:leanchat/data/channels/channel.dart';
import 'package:leanchat/feature/messages/messages_page.dart';

import 'channel_avatar.dart';

class ChannelListTile extends StatelessWidget {
  const ChannelListTile({Key key, @required this.channel}) : super(key: key);

  final Channel channel;

  @override
  Widget build(BuildContext context) {
    final titleStyle = TextStyle(fontWeight: FontWeight.w600, fontSize: 20);
    final subtitleStyle = TextStyle(fontWeight: FontWeight.w400, fontSize: 12);
    final membersSize = channel.members.length;
    final subtitle = membersSize != 0
        ? (membersSize == 1
            ? '${channel.members.first.name}'
            : '${channel.members.first.name} and ${membersSize - 1} others')
        : "";

    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.symmetric(vertical: 12),
            child: ListTile(
              onTap: () {
                Navigator.push<MessagesPage>(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MessagesPage(channelId: channel.id),
                  ),
                );
              },
              leading: ChannelAvatar(channel.photo),
              subtitle: Text(subtitle, style: subtitleStyle),
              title: Text(channel.name, style: titleStyle),
            ),
          ),
        ],
      ),
    );
  }
}
