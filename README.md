# LeanChat

A mobile app to contact with your best colleagues and spam them with GIFs.

## Launching

### VS Code

Example launch.json is provided below:

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Flutter",
            "request": "launch",
            "type": "dart",
            "program": "lib/feature/main.dart"
        }
    ]
}
```

## Deployment

Just run

```sh
flutter build ios --no-codesign --release -t lib/feature/main.dart --build-name=1.0.1 --build-number=1
flutter build apk --release -t lib/feature/main.dart --build-name=1.0.1 --build-number=1
```

Then you can use fastlane to sign and deploy iOS release app.

```sh
fastlane ios test
```